Description of Each class and method.

Decision Tree-
Train- Recursively iterates through each attribute in the vector, choosing the best attribute (the one with the most
information gain) to be a new sub tree each iteration. The sub trees contain branches which are indexed based off of the values
of the attribute. Through each recursive call, the list of attributes is shortened to remove the best attribute we
found previously. When the list is empty or each item in the list is the same outcome, a leaf is created with the
result.

Predict- Recursively iterates through the tree based off of the best attribute stored in each branch. The attribute index
indicates which attribute to compare in the instances passed in. The algorithm finds the next branch based off of the
dictionary index that is closest or equal to the value to compare. When a leaf is reached, the algorithm returns the
result.

NaiveBayes- 
Train- Creates a map of maps for each unique instance. Inside of this  all feature vectors are stored in the vertex 1.
The count of the number of occurences of the instance is stored in vertex 0. The overall length of the feature vector is
stored in vertex 2. The mean is stored in vertex 3, the standard deviation is stored in vertex 4, and the overall probability
is stored in vertex 5.

Predict- Probability of each feature existing in one of the classes is determined using the gaussian calculation.
We multiple the instance's we are trying to predict's probability of being in a certain class and store it in a map.
We then compare all the probability and determine the max, that is returned as our prediction.


Neural Network-
Train- Create two num py arrays, one containing ideal solutions the other containing the features as an array.
We have two weights one for the start layer and one for the hidden layer. We used the sigmoid function to determine
each individual weight at each layer (progressing to the output layer). We compare the weights at the output layer against
the ideal weight results. From here we correct the issue and repeat, until we reach a considerable large number of iterations.
50,000 loops seems to achieve near to a perfect result. When I did 100,000 there was a minscule difference so I considered this
a reasonable number of iterations. Additionally when examining Iris results on the train it returned only 1's.

Predict- We add each feature of the instance's feature vector to an array. We then use the sigmoid function using the dot product
between each layer's weight and the result. Essentially sending our result through each layer, until we arrive at a solution. 
We then compare this solution against known solutions. If we found the correct one return that one.
