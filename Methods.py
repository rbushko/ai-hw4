from abc import ABCMeta, abstractmethod
import math
import random
import numpy as np
# abstract base class for defining labels
class Label:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __str__(self): pass

       
class ClassificationLabel(Label):
    def __init__(self, label):
        #self.label_num = int(label)
        self.label_str = str(label)
        pass
        
    def __str__(self):
        return self.label_str
        pass

    def to_string(self):
        return self.label_str

# the feature vectors will be stored in dictionaries so that they can be sparse structures
class FeatureVector:
    def __init__(self):
        self.feature_vec = {}
        pass
        
    def add(self, index, value):
        self.feature_vec[index] = value
        pass
        
    def get(self, index):
        val = self.feature_vec[index]
        return val

    def len(self):
        return len(self.feature_vec)

    def get_vector(self):
        return self.feature_vec

    def to_string(self):
        string = ""
        for val in self.feature_vec:
            string += str(val) + ","
        return string

class Instance:
    def __init__(self, feature_vector, label):
        self._feature_vector = feature_vector
        self._label = label

    def to_string(self):
        string = self._label.to_string() + " " + self._feature_vector.to_string()
        return string

# abstract base class for defining predictors
class Predictor:
    __metaclass__ = ABCMeta

    @abstractmethod
    def train(self, instances): pass

    @abstractmethod
    def predict(self, instance): pass

class DecisionBranch:

    def __init__(self, attribute, branches=None):
        self.attribute = attribute
        self.branches = branches or {}

    def add(self, value, subtree):
        self.branches[value] = subtree

    def get(self, value):
        return self.branches[value]

    def get_attribute(self):
        return self.attribute

    def get_closest(self, value):
        # arbitrarily large number
        margin = 100000000.0
        closest = -1
        for branch in self.branches:
            if math.fabs(value - branch) < margin:
                margin = math.fabs(value - branch)
                closest = branch

        # print(str(closest))
        return closest

    def contains(self, key):
        return key in self.branches

    def empty(self):
        if len(self.branches) <= 0:
            return True
        return False

    def display(self, indent=0):
        print('')
        for (val, subtree) in self.branches.items():
            subtree.display(indent + 1)

    def __str__(self):
        return str(self.branches)

class DecisionLeaf:

    def __init__(self, result):
        self.result = result

    def get_result(self):
        return str(self.result)

    def display(self, indent=0, end=""):
        print('RESULT =', self.result)

    def __repr__(self):
        return repr(self.result)

class DecisionTree(Predictor):
    # TODO: implement pruning as outlined in 18.3.5
    # pruning is removing elements that are completely irrelevant
	# TODO: add option for pure information gain or an information gain ratio to split
    # TODO: great resource- http://www.onlamp.com/pub/a/python/2006/02/09/ai_decision_trees.html?page=1

    def __init__(self):
        self.tree = None

    def train(self, instances):
        attributes = instances[0]._feature_vector
        self.tree = self.dtl(instances, attributes, instances)
        return self.tree

    def dtl(self, examples, attributes, parent_examples):

        # base cases
        if len(examples) <= 0:
            return self.plurality_value(parent_examples)
        elif self.same_classification(examples):
            return DecisionLeaf(str(examples[0]._label))
        elif attributes.len() <= 0:
            return self.plurality_value(examples)
        else:
            # choose the best attribute
            best = self.choose_attribute(examples, attributes)
            tree = DecisionBranch(best)

            for val in self.get_values(examples, best):
                new_examples = self.examples_with_best(examples, best, val)
                new_attributes = self.remove_attribute(attributes, best)
                tree.add(val, self.dtl(new_examples, new_attributes, examples))

        return tree

    def remove_attribute(self, attributes, to_remove):
        vector = attributes.get_vector()
        new_attributes = FeatureVector()

        i = 0
        for v in vector:
            if i != to_remove:
                new_attributes.add(i, vector.get(i))
            i += 1
        return new_attributes

    # calculate disorder
    def entropy(self, examples, attribute):
        frequencies = {}
        data_entropy = 0.0

        for example in examples:
            if example._feature_vector.get(attribute) in frequencies:
                frequencies[example._feature_vector.get(attribute)] += 1.0
            else:
                frequencies[example._feature_vector.get(attribute)] = 1.0

        for frequency in frequencies.values():
            probability = frequency / len(examples)
            data_entropy += (-probability) * math.log(probability, 2)

        return data_entropy

    # find amount of info gain from using entropy
    def gain(self, examples, attribute):

        frequencies = {}
        sub_entropy = 0.0

        for example in examples:
            if example._feature_vector.get(attribute) in frequencies:
                frequencies[example._feature_vector.get(attribute)] += 1.0
            else:
                frequencies[example._feature_vector.get(attribute)] = 1.0

        for frequency in frequencies.keys():
            value = frequencies[frequency] / sum(frequencies.values())
            # print("Gain value: " + str(value))
            data = [example for example in examples if example._feature_vector.get(attribute) == value]
            sub_entropy = value * self.entropy(data, attribute)

        return self.entropy(examples, attribute) - sub_entropy

    def examples_with_best(self, examples, best, val):
        # TODO: fix this method so it works based off of the index of best
        new_examples = []
        # best is the best attribute
        for example in examples:
            if example._feature_vector.get(best) == val:
                new_examples.append(example)
        return new_examples

    def get_values(self, examples, best):
        values = []
        for example in examples:
            value = example._feature_vector.get(best)
            if not value in values:
                values.append(value)
        return values

    def choose_attribute(self, examples, attributes):
        max_gain = 0
        best = 0
        for attribute in attributes.get_vector():
            gain = self.gain(examples, attribute)
            # print(str(gain))
            if gain >= max_gain:
                max_gain = gain
                best = attribute
        return best

    def plurality_value(self, examples):
        # TODO: selects the most common output value among a set of examples, breaking ties randomly
        outputs = {}
        for example in examples:
            if not example._label in outputs:
                outputs[example._label] = 1
            else:
                outputs[example._label] += 1

        common = ""
        greatest = 0
        for key, value in outputs.items():
            if value > greatest:
                greatest = value
                common = key
            if value == greatest:
                tie = random.randint(0,1)
                if tie == 0:
                    common = key

        # print("Plurality value: " + common.to_string())
        return DecisionLeaf(str(common))

    def same_classification(self, examples):
        # get first classification
        classification = str(examples[0]._label)
        for example in examples:
            # if there's something in it with a different classification,
            # the data does not have all the same classifications
            if str(example._label) != classification:
                return False
        return True

    def predict(self, instances):
        prediction = self.get_prediction(instances._feature_vector, self.tree)
        return prediction

    def get_prediction(self, attributes, branch):

        if isinstance(branch, DecisionLeaf):
            return branch.get_result()

        # print("Branch attribute: " + str(branch.get_attribute()))
        value = attributes.get(branch.get_attribute())
        if not branch.contains(value):
            value = branch.get_closest(value)
        # print ("Branch: " + str(branch) + " Value: " + str(value))
        branch = branch.get(value)
        return self.get_prediction(attributes, branch)

class NaiveBayes(Predictor):
    # class code
    savedData = {}
    def __init__(self): pass
    def train(self, instances):
        instanceMap = {}
        count = 0
        for instance in instances:
            count = count + 1
            if instance._label.to_string() in instanceMap:
                instanceMap[instance._label.to_string()][0] +=1
                instanceMap[instance._label.to_string()][2] = instance._feature_vector.len()
                for x in range(0, instance._feature_vector.len()):
                    instanceMap[instance._label.to_string()][1] = np.append(instanceMap[instance._label.to_string()][1],instance._feature_vector.get(x))
            else:
                instanceMap[instance._label.to_string()] = {}
                instanceMap[instance._label.to_string()][0]= 1
                a = np.array(instance._feature_vector.get(0))
                for x in range(1, instance._feature_vector.len()):
                    a = np.append(a, instance._feature_vector.get(x))
                instanceMap[instance._label.to_string()][1] = a

        for fof in instanceMap:
            instanceMap[fof][1] =np.reshape(instanceMap[fof][1], (instanceMap[fof][0],instanceMap[fof][2]))
            instanceMap[fof][3] = np.mean(instanceMap[fof][1], 0)
            instanceMap[fof][4] = np.std(instanceMap[fof][1], 0)
            instanceMap[fof][5] = instanceMap[fof][0]/(count*1.0)
        self.savedData = instanceMap


    def gauss(self, x, mean, std):
        var = float(std) ** 2
        pi = math.pi
        denominator = (2 * pi * var) ** .5
        numumerator = math.exp(-(float(x) - float(mean)) ** 2 / (2 * var))
        return numumerator / denominator

    def predict(self, instance):
        maxProb = {}
        maxProbability = 0
        maximumString = ""
        for fof in self.savedData:
            maxProb[fof] = 1
            for x in range(0, instance._feature_vector.len()):
                maxProb[fof] = maxProb[fof] * self.gauss(instance._feature_vector.get(x), self.savedData[fof][3][x],self.savedData[fof][4][x])
            maxProb[fof] = maxProb[fof] * self.savedData[fof][5]
        for fof in maxProb:
            if maxProb[fof]>maxProbability:
                maxProbability = maxProb[fof]
                maximumString = fof
        return maximumString


class NeuralNetwork(Predictor):
    # class code
    network = {}
    count = {}
    startLayerWeight = {}
    hiddenLayerWeight = {}
    labelToNum = {}

    def __init__(self): pass
    def sigmoid(self, x):
        return 1.0/(1.0+np.exp(-x)*1.0)

    def sigmoid_prime(self, z):
        return self.sigmoid(z)* (1 - self.sigmoid(z))
    def train(self, instances):
        instanceMap = {}
        refArray ={}
        featureLength = 0
        refNum = 0
        count = 0
        for instance in instances:
            count = count + 1
            if instance._label.to_string() not in self.labelToNum:
                self.labelToNum[instance._label.to_string()] = refNum
                featureLength = instance._feature_vector.len()
                refNum +=1
            if count == 1:
                instanceMap = np.array(instance._feature_vector.get(0))
                refArray = np.array(self.labelToNum[instance._label.to_string()])
                for x in range(1, instance._feature_vector.len()):
                    instanceMap = np.append(instanceMap, instance._feature_vector.get(x))
            else:
                for x in range(0, instance._feature_vector.len()):
                    instanceMap = np.append(instanceMap, instance._feature_vector.get(x))
                refArray = np.append(refArray,self.labelToNum[instance._label.to_string()])

        instanceMap = np.reshape(instanceMap, (count, featureLength))
        refArray = np.reshape(refArray, (count, 1))
        np.random.seed(1)
        # Weights are initialize randomly between -0.01 and 0.01 uniformly
        #self.startLayerWeight = 2.0 * np.random.random((featureLength, count)) - 1.0
        #self.HiddenLayerWeight = 2.0  * np.random.random((count, 1)) - 1.0
        # Weights are initialize using the scheme i.e. 1/sqrt(n)
        self.startLayerWeight = (refNum-1) *(2/math.sqrt(count) * np.random.random((featureLength, count)) - 1/math.sqrt(count))
        self.HiddenLayerWeight = (count*count*1.0)*((2.0/(count*1.0)) * np.random.random((count, 1)) - 1/(count*1.0))


        Input = instanceMap
        Output = refArray

        for j in xrange(10000):
            #-----------------Setting up the layers. Like Onions!-------
            Layer0 = Input
            Layer1 = self.sigmoid(np.dot(Layer0, self.startLayerWeight))
            Layer2 = self.sigmoid(np.dot(Layer1, self.HiddenLayerWeight))
            #---------------Error/Back Propagation----------------------
            layer2_error = Output - Layer2
            l2_delta = layer2_error * self.sigmoid_prime(Layer2)
            l1_error = l2_delta.dot(self.HiddenLayerWeight.T)
            l1_delta = l1_error * self.sigmoid_prime(Layer1)
            #-------------Update-----------------------------
            self.HiddenLayerWeight += np.dot(2*Layer1.T,l2_delta)
            self.startLayerWeight += np.dot(2*Layer0.T, l1_delta)

        self.labelToNum = self.labelToNum

    def predict(self, instance):
        result = np.array(instance._feature_vector.get(0))
        for x in range(1, instance._feature_vector.len()):
            result = np.append(result,instance._feature_vector.get(x))

        result = self.sigmoid(np.dot(result, self.startLayerWeight))
        result = self.sigmoid(np.dot(result, self.HiddenLayerWeight))
        result = round(result)
        for fof in self.labelToNum:
            if self.labelToNum[fof] == result:
                return fof





"""
TODO: you must implement additional data structures for
the three algorithms specified in the hw4 PDF

for example, if you want to define a data structure for the
DecisionTree algorithm, you could write

class DecisionTree(Predictor):
	# class code

Remember that if you subclass the Predictor base class, you must
include methods called train() and predict() in your subclasses
"""
